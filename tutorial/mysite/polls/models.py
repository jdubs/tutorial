import datetime
from django import forms
from django.forms import ModelForm
from django.utils import timezone
from django.db import models

class Poll(models.Model):
	question = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	createdBy = models.CharField(max_length=24)

	def __unicode__(self):
		return self.question

	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

	was_published_recently.admin_order_field = 'pub_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'

class Choice(models.Model):
	poll = models.ForeignKey(Poll)
	choice = models.CharField(max_length=200)
	votes = models.IntegerField()
	createdBy = models.CharField(max_length=24)

	def __unicode__(self):
		return self.choice

class PollForm(ModelForm):
	#question = forms.CharField(max_length=100)
	class Meta:
		model = Poll
		exclude = ["pub_date"]

def add_poll(request):
    """Add a new poll."""
    p = request.POST

    if p.has_key("question") and p["question"]:
        createdby = request.META['REMOTE_ADDR']
        poll = Poll()
        cf = CommentForm(p, instance=poll)
        cf.fields["author"].required = False

        poll = cf.save(commit=False)
        poll.pub_date = datetime.datetime.now()
        poll.save()
    return HttpResponseRedirect(reverse("dbe.blog.views.post", args=[pk]))
