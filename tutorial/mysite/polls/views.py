from django.shortcuts import get_object_or_404, render_to_response
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext
from polls.models import Choice, Poll, PollForm
import datetime

def vote(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the poll voting form.
        return render_to_response('polls/detail.html', {
            'poll': p,
            'error_message': "You didn't select a choice.",
        }, context_instance=RequestContext(request))
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('poll_results', args=(p.id,)))

def detail(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    return render_to_response('polls/detail.html', {'poll': p}, context_instance=RequestContext(request))

def newPoll(request):
	if request.method == 'POST':
		form = PollForm(request.POST)
		if form.is_valid():
			form.save(createdBy = request.META['REMOTE_ADDR'], pub_date = datetime.datetime.now(), question = form.cleaned_data['question'])
			return HttpResponseRedirect('/polls/')
	else:
		form = PollForm()

	return render_to_response('polls/newPoll.html', {'form': form}, context_instance=RequestContext(request))
